#include<stdio.h>
#include<graphics.h>
#include<conio.h>
#include<stdlib.h>
#include<mmsystem.h>
#include<time.h>
#pragma comment(lib,"winmm.lib")
/*
a simple snake project for presentation
*/

#define SNAKE_NUM 500 //MAX snake length
enum DIR 
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
};
//snake structure
struct Snake 
{
	int size;  //snake length
	int dir;   //moving direction
	int speed; //movement speed
	int gamespeed;
	POINT coor[SNAKE_NUM];//coordinate long type

}snake;

//food structure
struct Food
{
	int x;
	int y;
	int r;     //food size
	bool flag; //eaten flag
	DWORD color; //color
}food;
//init data
void GameInit() 
{
	//play music

	mciSendString("open ./Laventale.mp3 alias BGM", 0, 0, 0);
	mciSendString("setaudio BGM volume to 100", NULL, 0, 0);
	mciSendString("play BGM repeat", 0, 0, 0);



	initgraph(800, 600, 0);


	//init snake data
	srand((unsigned)time(NULL));
	snake.size = 3;
	snake.speed = 10;
	snake.dir= RIGHT;
	snake.gamespeed = 80;
	for(int i= 0;i <snake.size;i++)
	{
		snake.coor[i].x = 40-10*i;
		snake.coor[i].y = 10;
		//printf("?%d %d", snake.coor[i].x, snake.coor[i].y);
	}
	//init food data
	//We need stdlib.h to do this
	food.x = rand() % 800;
	food.y = rand() % 600;
	food.color = RGB(rand() % 256, rand() % 256, rand() % 256);
	food.r = rand() % 10 + 5;
	food.flag = true;
}

void GameDraw() 
{
	// BatchDraw to prevent splash screen
	BeginBatchDraw();
	//Init background color
	setbkcolor(RGB(29, 111, 110));
	cleardevice();
	//draw snake
	setfillcolor(GREEN);
	for (int i = 0; i < snake.size; i++) 
	{
		solidcircle(snake.coor[i].x, snake.coor[i].y, 5);
	}
	//draw food
	if (food.flag) 
	{
		solidcircle(food.x, food.y, food.r);
	}
	EndBatchDraw();
}
//snake movement
void snakeMove() 
{

	//snake follow the snake head
	for (int i = snake.size -1; i > 0 ; i--)
	{
		snake.coor[i] = snake.coor[i - 1];
	}
	//change in coordinate
	switch (snake.dir) 
	{
	case UP:
		snake.coor[0].y-=snake.speed;
		if(snake.coor[0].y + 10 <= 0)//over boundary
		{
			snake.coor[0].y = 600;
		}
		break;
	case DOWN:
		snake.coor[0].y+= snake.speed;
		if (snake.coor[0].y - 10 >= 600)//over boundary
		{
			snake.coor[0].y = 0;
		}
		break;
	case LEFT:
		snake.coor[0].x-= snake.speed;
		if (snake.coor[0].x + 10 <= 0)//over boundary
		{
			snake.coor[0].x = 800;
		}
		break;
	case RIGHT:
		snake.coor[0].x+= snake.speed;
		if (snake.coor[0].x - 10 >= 800)//over boundary
		{
			snake.coor[0].x = 0;
		}
		break;
	}



}
//press button to change snake direction
void keyControl() 
{	
	//check if user hit key
	if (_kbhit()) 
	{
		//72 80 75 77 are ↑,↓,←,→
		switch (_getch())
		{
		case 'w':
		case 'W':
		case 72:
			//change snake direction
			if(snake.dir!=DOWN)
				snake.dir = UP;
			break;
		case 's':
		case 'S':
		case 80:
			if (snake.dir != UP)
				snake.dir = DOWN;
			break;
		case 'a':
		case 'A':
		case 75:
			if (snake.dir != RIGHT)
				snake.dir = LEFT;
			break;
		case 'd':
		case 'D':
		case 77:
			if (snake.dir != LEFT)
				snake.dir = RIGHT;
			break;
		case ' ':
			while (1) 
			{
				if (_getch() == ' ')
					return;
			}
			break;
		}
	}

	
}
void Eatfood() 
{
	if (food.flag && snake.coor[0].x >= food.x-food.r && snake.coor[0].x <= food.x+food.r &&
		snake.coor[0].y >= food.y - food.r && snake.coor[0].y <= food.y + food.r)
	{
		food.flag = false;
		snake.size++;
		if (snake.gamespeed >= 40)
			snake.gamespeed -= 5;
	}
	//if food is eaten refresh a food
	if (!food.flag) 
	{
		food.x = rand() % 800;
		food.y = rand() % 600;
		food.color = RGB(rand() % 256, rand() % 256, rand() % 256);
		food.r = rand() % 10 + 5;
		food.flag = true;
	}
	/*game point*/

}
//game pause
bool gameover() 
{
	for (int i = 1; i < snake.size; i++) 
	{
		if (snake.coor[0].x == snake.coor[i].x && snake.coor[0].y == snake.coor[i].y)
			return true;
	}
	return false;
}
int main()
{

	GameInit();
	while (1)
	{
		snakeMove();
		GameDraw();
		keyControl();
		Eatfood();
		Sleep(snake.gamespeed);
		if (gameover()) 
		{
			GameInit();
		}
	}
	return 0;
}
